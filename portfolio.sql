-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 23 2015 г., 17:05
-- Версия сервера: 5.5.45
-- Версия PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `portfolio`
--

-- --------------------------------------------------------

--
-- Структура таблицы `administrators`
--

CREATE TABLE IF NOT EXISTS `administrators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `administrators_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `administrators`
--

INSERT INTO `administrators` (`id`, `username`, `password`, `name`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$ZlbFRMPXqNckPaGg6jNMyuxRKAqS.Y26fjSz.XLNZkDec8h/SH0s.', 'admin', NULL, '2015-11-23 12:05:14', '2015-11-23 12:05:14');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_10_12_104748_create_administrators_table', 1),
('2015_10_25_211512_create_projects_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `price` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `price`, `time`, `year`, `created_at`, `updated_at`) VALUES
(1, 447, 3138, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(2, 887, 2421, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(3, 182, 4482, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(4, 222, 3487, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(5, 294, 3179, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(6, 284, 2826, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(7, 646, 2333, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(8, 321, 1910, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(9, 488, 1235, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14');

-- --------------------------------------------------------

--
-- Структура таблицы `project_screens`
--

CREATE TABLE IF NOT EXISTS `project_screens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `project_screens`
--

INSERT INTO `project_screens` (`id`, `project_id`, `filename`, `width`, `height`, `created_at`, `updated_at`) VALUES
(1, 1, 'background3.jpg', 0, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(2, 2, 'background1.jpg', 0, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(3, 3, 'background3.jpg', 0, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(4, 4, 'background2.jpg', 0, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(5, 5, 'background4.jpg', 0, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(6, 6, 'background4.jpg', 0, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(7, 7, 'background1.jpg', 0, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(8, 8, 'background1.jpg', 0, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(9, 9, 'background1.jpg', 0, 0, '2015-11-23 12:05:14', '2015-11-23 12:05:14');

-- --------------------------------------------------------

--
-- Структура таблицы `project_tags`
--

CREATE TABLE IF NOT EXISTS `project_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `project_trans`
--

CREATE TABLE IF NOT EXISTS `project_trans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `locale` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `project_trans_locale_index` (`locale`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Дамп данных таблицы `project_trans`
--

INSERT INTO `project_trans` (`id`, `project_id`, `locale`, `name`, `text`, `created_at`, `updated_at`) VALUES
(1, 1, 'uk', '3dFaAUuo CUtw', 'XS5Rva m2iJy 05ZHeAce.', '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(2, 1, 'en', '7s030 muYLrtCm', 'dON0KGTJ svkGyv2 roTxgJiH.', '2015-11-23 12:05:14', '2015-11-23 12:05:14'),
(3, 1, 'ru', '9JkvCLdK zm6s', 'bza4oyUY uuJZCMuc 6INLn9.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(4, 2, 'uk', 'RXOd Nz6nNn', 'RIZgY syWv t2uxU6R.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(5, 2, 'en', 'wIfWR Jx0Q', 'yyPVpFjj zwNrRQ9 Qwqf.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(6, 2, 'ru', 'DYxC2C TW5qWIHa', 'NavAjw0v b5WK9QA 2gPjS.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(7, 3, 'uk', 'JxdZYQzU ZTE6wX7', 'xSkvYM KVxe mnBZLv.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(8, 3, 'en', 'hQGZO9a TYSoKeju', 'fLZ93x zaB0 emAegQ.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(9, 3, 'ru', 'KlEJ Y4xAJL', 'UO7M RKWfFmdc lGXEy5Vf.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(10, 4, 'uk', 'qXNtoJ13 vdrj58F', 'Xqi4u4Y hQVEIty zewyzjqn.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(11, 4, 'en', 'Lt0o Ix1iXb', 'k2i91Nof Tib6aF chap.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(12, 4, 'ru', 'pXufT DUjpnKk', 'Cv4bII iNUJcJ 2bdYMPK.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(13, 5, 'uk', 'SwFX cgi8LRF', 'k0R2xJ h9NhL OiQFLt.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(14, 5, 'en', 'QEnCl aeV6', 'teD6l7L pj9IH2S bloc8dIq.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(15, 5, 'ru', 'v8K58Xib 9ffC', 'erFHVDX Zby1614 3ZSFTOCv.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(16, 6, 'uk', 'QZRWEzPW eVN66', 'tMD4 9RPsGyVM 3MJk9.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(17, 6, 'en', 'pzhw seEm', 'zEW0pSO WTpyVY YniEQI.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(18, 6, 'ru', '09rj O5e2uFV', 'c9uDWm7Q 1vYKep lh3IJ.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(19, 7, 'uk', 'YzkF3nTE EfkxGw', 'roQXzuE1 WHf5Iq NazTS.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(20, 7, 'en', 'NS315RxG pEbWd', 'EunBGc figaN nIYxV2.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(21, 7, 'ru', 'v8jDlkrv Zmzw1H', 'iQ9Prg kH1A H4o6Fk1.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(22, 8, 'uk', 'CJ0uyDa DmOGlom', '0gGd8CIZ Sy9C7DuF ni2SJ1TH.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(23, 8, 'en', 'PboB w4og', 'oydTd ECBrf6w RMR3P.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(24, 8, 'ru', 'Z8Mycf6X FKDQm', 'OuEf AILyg2 3f8S.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(25, 9, 'uk', 'NVCPbm moJYBU', 'qtDqXpi9 sbyULYt7 yTtn.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(26, 9, 'en', 'IN25ml SPrhwdf', '9VIPW3 wCkF13PW 3Hq72qPE.', '2015-11-23 12:05:15', '2015-11-23 12:05:15'),
(27, 9, 'ru', 'G07D7 m0Wkm', 'pFhP 79W9JP1Z wcvC.', '2015-11-23 12:05:15', '2015-11-23 12:05:15');

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
