<?php

use GrahamCampbell\Markdown\Facades\Markdown;

/**
 * @var $article \App\Articles
 * @class Markdown GrahamCampbell\Markdown\Facades\Markdown
 */
?>


@extends('layouts.main')

<?php $title = $article->name ?>
@section('title', $title)

@section('pageH1', $title)
@section('pageH1Class', 'article-title page-desc')

@section('pageDesc', $article->short_desc)

@section('top-buttons')

    @foreach($article::getTagsByArticle($article->id) as $tag)
        <a href="{{ urlTo('/blog/tag/'.$tag->name) }}"
           class="btn waves-effect waves-light blue lighten-1 hide-on-med-and-down">{{ $tag->name }}</a>
    @endforeach

@endsection

@section('description', $article->meta_description)
@section('keywords', $article->meta_keywords)

@section('content')
    <div class="container article-text">
        {!! Markdown::convertToHtml($article->text)  !!}
    </div>
@stop