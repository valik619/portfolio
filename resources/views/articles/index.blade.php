@extends('layouts.main')

@section('title', trans('links.BLOG'))
@section('pageH1', trans('links.BLOG'))
@section('pageDesc', trans('links.BLOG_PAGE'))

@section('top-buttons')
    <a href="{{ urlTo('/blog/tag/yii') }}" id="download-button"
       class="btn-large waves-effect waves-light blue darken-3">Yii</a>
    <a href="{{ urlTo('/blog/tag/laravel') }}" id="download-button"
       class="btn-large waves-effect waves-light blue darken-3">Laravel</a>
@stop

@section('content')
    <div class="container">
        <div class="row">
            @foreach($model as $arr)
                <?php $screen = $arr->screen->first(); ?>
                <div class="col s12">
                    <div class="card">
                        <a href="{{ urlTo('/blog/article/'.$arr->id) }}">
                            <div class="article-header bg-darken-blue">{{ $arr->name }}</div>
                        </a>
                        <div class="card-content">
                            <p>{{ $arr->short_desc }}</p>
                        </div>

                        <div class="card-action">
                            <a href="{{ urlTo('/blog/article/'.$arr->id) }}"
                               class="blue-text text-darken-1">{{ trans('links.READ_ARTICLE') }}</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop