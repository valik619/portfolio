<div class="row">

    @if(Session::has('message'))
        <div class="alert alert-info" role="alert">{{ Session::get('message') }}</div>
    @endif

    <?php $locales = []; ?>

    <form method="POST" action="{{ action('Admin\Portfolio\IndexController@create') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        <label for="price">Price $:</label><br>
        <input id="price" type="number" name="price"><br>

        <label for="year">Year:</label><br>
        <input id="year" type="number" name="year"><br>

        <label for="time">Dev Time:</label><br>
        <input id="time" type="number" name="time"><br>

        <label for="time_type">Time type:</label>
        <select name="time_type" id="time_type">
            <option>H</option>
            <option>D</option>
            <option>M</option>
        </select><br>

        <input type="submit" class="btn btn-primary col-xs-12" value="Create"/>
    </form>

</div>