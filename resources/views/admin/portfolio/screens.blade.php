<a href="{{ url('admin/projects/'.$id.'/screens/new') }}" class="btn btn-primary"><i
            class="fa fa-plus"></i> New</a>

<div class="row">

    @foreach($model as $arr)

        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="thumbnail">

                <img class="opacity-8"
                     src="{{ ImageManager::getImagePath(public_path() . '/screens/'.$id.'/' . $arr->filename, 480, 260, 'crop') }}">


                <div class="caption">
                    <h3>{{ $arr->filename }}</h3>

                    <p>
                        <a href="{{ url('admin/projects/'.$arr->id.'/screens/new') }}" class="btn btn-primary"><i
                                    class="fa fa-plus"></i> New</a>

                        <a href="{{ url('admin/projects/'.$arr->id).'/screens/delete' }}" data-method="delete"
                           class="btn btn-danger pull-right"><i class="fa fa-times"></i> Delete</a>
                    </p>

                </div>
            </div>
        </div>


    @endforeach
    <div class="clearfix"></div>

</div>