<div class="row">

    @foreach($model as $arr)

        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="thumbnail">

                <div class="caption">
                    <h3>{{ $arr->name }}</h3>

                    <p>
                        <a href="{{ url('admin/tags/'.$arr->id.'/edit') }}" class="btn btn-primary"><i
                                    class="fa fa-edit"></i> Edit</a>

                        <a href="{{ url('admin/tags/'.$arr->id).'/destroy' }}" data-method="delete"
                           class="btn btn-danger pull-right"><i class="fa fa-times"></i> Delete</a>
                    </p>

                </div>
            </div>
        </div>

    @endforeach
    <div class="clearfix"></div>

</div>