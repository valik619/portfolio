<div class="row">

    @foreach($model as $arr)

        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="thumbnail">

                <a href="{{ url('admin/projects/'.$arr->id.'/screens') }}">
                    @if(!empty($arr->screen->first()))
                        <img class="opacity-8"
                             src="{{ ImageManager::getImagePath(public_path() . '/screens/'.$arr->id.'/' . $arr->screen->first()->filename, 480, 260, 'crop') }}">
                    @else
                        <img class="opacity-8"
                             src="{{ ImageManager::getImagePath(public_path() . '/background1.jpg', 480, 260, 'crop') }}">
                    @endif
                </a>

                <div class="caption">
                    <h3>{{ $arr->name }}</h3>

                    <p>{{ $arr->short_desc }}</p>

                    <p>
                        <a href="{{ url('admin/projects/'.$arr->id.'/edit') }}" class="btn btn-primary"><i
                                    class="fa fa-edit"></i> Edit</a>

                        <a href="{{ url('admin/projects/'.$arr->id).'/destroy' }}" data-method="delete"
                           class="btn btn-danger pull-right"><i class="fa fa-times"></i> Delete</a>
                    </p>

                </div>
            </div>
        </div>


    @endforeach
    <div class="clearfix"></div>

</div>