<div class="row">

    @if(Session::has('message'))
        <div class="alert alert-info" role="alert">{{ Session::get('message') }}</div>
    @endif

    <?php $locales = []; ?>

    <form method="POST" action="{{ action('Admin\Blog\IndexController@update', ['id' => $id]) }}">

        <div class="row">
            <div class="col-xs-12">
                <label for="tags">Tags:</label><br>
                <select id="tags" name="tags[]" size="{{ count($all_tags) }}" class="form-control" multiple>
                    @foreach($all_tags as $arr)
                        <option value="{{ $arr->id }}" @if(in_array($arr->id, $tags)) selected @endif>{{ $arr->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <hr>

        <div class="clearfix"></div>

        <div class="row">
            @foreach($model as $arr)
                <div class="col-md-12 col-sm-12 col-lg-12">

                    <div class="thumbnail">

                        <div class="caption">
                            <h3>{{ $arr->locale }}</h3>

                            <p>
                                <input class="form-control" type="text" name="name[{{ $arr->locale }}]"
                                       value="{{ $arr->name }}" placeholder="Title"/><br/>

                                <input class="form-control" type="text" name="short_desc[{{ $arr->locale }}]"
                                       value="{{ $arr->short_desc }}" placeholder="Short description"/><br/>

                                <input class="form-control" type="text" name="meta_keywords[{{ $arr->locale }}]"
                                       value="{{ $arr->meta_keywords }}" placeholder="meta keywords"/><br/>

                                <input class="form-control" type="text" name="meta_description[{{ $arr->locale }}]"
                                       value="{{ $arr->meta_description }}" placeholder="meta description"/><br/>

                            <textarea class="form-control table-responsive"
                                      name="text[{{ $arr->locale }}]" placeholder="Article text">{{ $arr->text }}</textarea>
                            </p>

                            <p>
                                <a href="{{ url('admin/articles/delete-translate', ['id' => $arr->id]) }}"
                                   class="btn btn-danger">
                                    <i class="fa fa-times"></i>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>

                <?php $locales[] = $arr->locale; ?>

            @endforeach
        </div>
        <div class="clearfix"></div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="_method" value="PUT"/>

        <input type="submit" class="btn btn-primary col-sm-3" value="Save"/>
    </form>

    <form class="col-xs-9" method="POST"
          action="{{ route('admin.Articles.AddTranslate', ['id' => $id]) }}">

        <div class="col-xs-6">
            <select name="locale" class="form-control">
                @foreach(Config::get('app.locales') as $locale)
                    @if(!in_array($locale, $locales))
                        <option value="{{ $locale }}">{{ $locale }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        <input type="submit" class="btn btn-danger col-xs-6" value="Add"/>

    </form>
</div>