<div class="row">

    @if(Session::has('message'))
        <div class="alert alert-info" role="alert">{{ Session::get('message') }}</div>
    @endif

    <form method="POST" action="{{ action('Admin\Blog\TagController@save', ['id' => $id]) }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        <label for="name">Name:</label><br>
        <input class="form-control" id="name" type="text" name="name" value="{{ $model->name }}"><br>

        <input type="submit" class="btn btn-primary col-xs-12" value="Save"/>
    </form>

</div>