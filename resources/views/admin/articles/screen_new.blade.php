<div class="row">

    @if(Session::has('message'))
        <div class="alert alert-info" role="alert">{{ Session::get('message') }}</div>
    @endif

    <form class="col-xs-12" method="POST" action="{{ route('admin.Articles.ScreenSave', ['id' => $id]) }}" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="{{csrf_token()}}"/>

        <input type="file" class="form-control" name="screen[]" multiple />

        <hr>

        <input type="submit" class="btn btn-danger col-xs-12" value="Add"/>

    </form>
</div>