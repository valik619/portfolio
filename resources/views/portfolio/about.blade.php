@extends('layouts.main')

@section('title', trans('links.ABOUT_ME'))
@section('pageH1', 'Web-Developer')
@section('pageDesc', trans('links.ABOUT_PAGE'))

@section('top-buttons')
    <a href="#" id="download-button" class="btn-large waves-effect waves-light blue lighten-1">TW</a>
    <a href="#" id="download-button" class="btn-large waves-effect waves-light blue darken-2">VK</a>
    <a href="#" id="download-button" class="btn-large waves-effect waves-light blue darken-3">FB</a>
@stop

@section('content')
    <div class="container">

        <h3>{{ trans('links.ABOUT_ME') }}</h3>

        {{ trans('aboutText.1') }}

    </div>
@stop