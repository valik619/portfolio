@extends('layouts.main')

@section('title', 'MainPage')
@section('pageH1', 'Web-Developer')
@section('pageDesc', trans('links.WELCOME'))

@section('top-buttons')
    <a href="#examples" class="btn-large waves-effect waves-light blue darken-2">{{ trans('links.EXAMPLES') }}</a>
    <a href="#skills" class="btn-large waves-effect waves-light green darken-2">{{ trans('links.SKILLS') }}</a>
@stop

@section('content')
    <div id="examples" class="container scrollspy">
        <div class="row">

            <h2>{{ trans('links.LAST_WORKS') }}</h2>

            <?php $i = 0 ?>
            @foreach($model as $arr)
                <div class="col l4 s12 m{{ $i++ === 2 ? '12' : '6' }}">
                    <div class="card">
                        <div class="card-image darken-5">
                            <img class="opacity-8"
                                 src="{{ ImageManager::getImagePath(public_path() . '/screens/'.$arr->id.'/' . $arr->screen->first()->filename, 480, 260, 'crop') }}">
                            <span class="card-title page-desc">{{ $arr->name }}</span>
                        </div>
                        <div class="card-content">
                            <p class="truncate">{{ $arr->short_desc }}</p>
                        </div>
                        <div class="card-action">
                            <a href="{{ Linguist::url('/portfolio/'.$arr->id) }}" class="blue-text text-darken-1">{{ trans('links.INFO') }}</a>
                        </div>
                    </div>
                </div>

            @endforeach

            <div class="clearfix"></div>

            <div class="center-align center-block">
                <a href="{{ Linguist::url('/portfolio') }}" class="btn-large waves-effect waves-light blue darken-3">{{ trans('links.MORE') }}</a>
            </div>


        </div>
    </div>

    <div id="skills" class="container scrollspy">
        <div class="row">

            <h2>{{ trans('links.SKILLS') }}</h2>

            <div class="col s12 m4">
                <h5>{{ trans('links.LANGUAGES') }}</h5>

                <ul>
                    <li><a href="{{ Linguist::url('/portfolio/tag/php') }}">PHP</a></li>
                    <li><a href="{{ Linguist::url('/portfolio/tag/javascript') }}">JavaScript</a></li>
                </ul>

            </div>

            <div class="col s12 m4">
                <h5>{{ trans('links.FRAMEWORKS') }}</h5>

                <ul>
                    <li><a href="{{ Linguist::url('/portfolio/tag/yii') }}">Yii Framework</a></li>
                    <li><a href="{{ Linguist::url('/portfolio/tag/laravel') }}">Laravel 5</a></li>
                </ul>
            </div>

            <div class="col s12 m4">
                <h5>{{ trans('links.WEB_TECH') }}</h5>

                <ul>
                    <li><a href="{{ Linguist::url('/portfolio/tag/html') }}">HTML 5</a></li>
                    <li><a href="{{ Linguist::url('/portfolio/tag/css') }}">CSS 3</a></li>
                </ul>
            </div>

            <div class="col s12">
                <h5>{{ trans('links.OTHER_SKILLS') }}</h5>

                <div class="row">

                    <div class="col s12 m4">
                        <h6>{{ trans('links.JS_FRAMEWORKS') }}</h6>

                        <ul>
                            <li><a href="{{ Linguist::url('/portfolio/tag/jquery') }}">jQuery</a></li>
                            <li><a href="{{ Linguist::url('/portfolio/tag/photoswipe') }}">PhotoSwipe</a></li>
                            <li><a href="{{ Linguist::url('/portfolio/tag/framework7') }}">Framework7</a></li>
                        </ul>
                    </div>

                    <div class="col s12 m4">
                        <h6>{{ trans('links.CSS_FRAMEWORKS') }}</h6>

                        <ul>
                            <li><a href="{{ Linguist::url('/portfolio/tag/bootstrap') }}">Bootstrap</a></li>
                            <li><a href="{{ Linguist::url('/portfolio/tag/materialize') }}">Materialize</a></li>
                        </ul>
                    </div>

                    <div class="col s12 m4">
                        <h6>{{ trans('links.CMS') }}</h6>

                        <ul>
                            <li><a href="{{ Linguist::url('/portfolio/tag/joomla') }}">Joomla 2.5</a></li>
                            <li><a href="{{ Linguist::url('/portfolio/tag/opencart') }}">OpenCart</a></li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>
    </div>
@stop