@extends('layouts.main')

<?php $title = $model->name ?>
@section('title', $title)
@section('pageH1', $title)
@section('pageDesc', trans('links.PROJECT_PAGE', ['name' => $model->name]))

@section('description', $model->meta_description)
@section('keywords', $model->meta_keywords)

@section('top-buttons')
    <a href="#screenshots" id="download-button" class="btn-large waves-effect waves-light blue darken-2">
        {{ trans('links.SCREENS') }}
    </a>
    <a href="#text" id="download-button" class="btn-large waves-effect waves-light blue darken-3">
        {{ trans('links.DESCRIPTION') }}
    </a>

@stop

@section('head')
    <link rel="stylesheet" href="{{ asset('gallery/photoswipe.css') }}">
    <link rel="stylesheet" href="{{ asset('gallery/default-skin/default-skin.css') }}">
@endsection

<?php
$width = 1366;
$ratio = $model->screen->first()->height / $model->screen->first()->width;
$height = round($width * $ratio);

$med = 0.6;

?>

@section('content')
    <div class="container">
        <div class="row">

            <div class="col l12">
                <div class="card">
                    <div class="card-image darken-5">
                        <img class="opacity-8 projectscreen"
                             src="{{ ImageManager::getImagePath(public_path() . '/screens/'.$model->id.'/' . $model->screen->first()->filename,
                             $width,
                             $height,
                             'crop') }}">

                        <span class="card-title page-desc">{{ $model->name }}</span>
                    </div>
                    <div class="card-content">

                        <div class="row scrollspy" id="screenshots">

                            <div id="demo-test-gallery" class="demo-gallery" data-pswp-uid="1">

                                <?php $i = 0; ?>
                                @foreach($model->screen as $screen)


                                    <a href="{{ ImageManager::getImagePath(public_path() . '/screens/'.$model->id.'/' . $screen->filename, $screen->width, $screen->height, 'crop') }}"
                                       data-size="{{ $screen->width.'x'.$screen->height }}"
                                       data-med="{{ ImageManager::getImagePath(public_path() . '/screens/'.$model->id.'/' . $screen->filename, round($screen->width*$med), round($screen->height*$med), 'crop') }}"
                                       data-med-size="{{  round($screen->width*$med).'x'. round($screen->height*$med) }}"
                                       data-author="Folkert Gorter" class="demo-gallery__img--main col l4 m6 s12">
                                        <img src="{{ ImageManager::getImagePath(public_path() . '/screens/'.$model->id.'/' . $screen->filename, 400, 240, 'crop') }}"
                                             alt="" class="responsive-img">
                                    </a>


                                    <?php $i++; ?>
                                @endforeach

                            </div>

                        </div>

                        <span id="text" class="scrollspy">
                            {{ $model->text }}
                        </span>
                    </div>
                    <div class="card-action">
                        <a href="{{ Linguist::url('/portfolio/'.$model->id) }}"
                           class="blue-text text-darken-1">{{ trans('links.SHARE') }}</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop

@section('body')

@include('gallery')

@endsection