@extends('layouts.main')

@section('title', trans('links.ABOUT_ME'))
@section('pageH1', 'Web-Developer')
@section('pageDesc', trans('links.CONTACTS_PAGE'))

@section('top-buttons')
    <a target="_blank" href="http://twitter.com/valik619" id="download-button" class="btn-large waves-effect waves-light blue lighten-1">TW</a>
    <a target="_blank" href="http://vk.com/sv_backend1" id="download-button" class="btn-large waves-effect waves-light blue darken-2">VK</a>
    <a target="_blank" href="https://www.facebook.com/SaikValentyn" id="download-button" class="btn-large waves-effect waves-light blue darken-3">FB</a>
@stop

@section('content')
    <div class="container">

        <h3>{{ trans('links.CONTACTS') }}</h3>

        {{ trans('contactsText.1') }}

    </div>
@stop