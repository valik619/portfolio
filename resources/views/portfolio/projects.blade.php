@extends('layouts.main')

@section('title', 'About Me')
@section('pageH1', trans('links.EXAMPLES'))
@section('pageDesc', trans('links.EXAMPLES_PAGE'))

@section('top-buttons')
    <a href="{{ urlTo('/portfolio/tag/yii') }}" class="btn-large waves-effect waves-light blue darken-1">Yii</a>
    <a href="{{ urlTo('/portfolio/tag/laravel') }}" class="btn-large waves-effect waves-light blue darken-1">Laravel</a>
@stop

@section('content')
    <div class="container">
        <div class="row">
            @foreach($model as $arr)
            <div class="col m6 s12">
                <div class="card">
                    <div class="card-image darken-5">
                        <img class="opacity-8"
                             src="{{ image('/screens/'.$arr->id.'/' . $arr->screen->first()->filename, 480, 260) }}">
                        <span class="card-title page-desc">{{ $arr->name }}</span>
                    </div>
                    <div class="card-content">
                        <p>{{ $arr->short_desc }}</p>
                    </div>
                    <div class="card-action">
                        <a href="{{ Linguist::url('/portfolio/'.$arr->id) }}" class="blue-text text-darken-1">{{ trans('links.INFO') }}</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@stop