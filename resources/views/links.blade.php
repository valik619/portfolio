<?php
$uri = explode('/', substr(request()->url(), strlen('http://')), 2);
if (!isset($uri[1])) $uri[1] = '';
?>

<li><a href="{{ Linguist::url('about') }}">{{ trans('links.ABOUT') }}</a></li>

<li>
    <a href="{{ Linguist::url('contacts') }}">
        {{ trans('links.CONTACTS') }}
    </a>
</li>

<li><a href="{{ Linguist::url('blog') }}">{{ trans('links.BLOG') }}</a></li>

@foreach($app->config->get('app.locales') as $lang)
    @if($lang != App::getLocale())
        <li>
            <a href="{{ url($lang == 'uk' ? request()->url() : $lang.'/'.$uri[1]) }}">
                <img src="{{ asset('/images/icons/'.$lang.'.png') }}" style="vertical-align: middle; height: 24px;"/>
            </a>
        </li>
    @endif
@endforeach