<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>V. Sayik Portfolio - @yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="author" content="Valentin Sayik">

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
    @yield('head')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</head>
<body>
<nav class="bg-darken-blue" role="navigation">
    <div class="nav-wrapper container">
        <a id="logo-container" href="{{ Linguist::url('/') }}" class="brand-logo">{{ trans('links.NAME') }}</a>
        <ul class="right hide-on-med-and-down">
            @include('links')
        </ul>

        <ul id="nav-mobile" class="side-nav">
            @include('links')
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>

<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
        <div class="container">
            <br><br>
            <h1 class="header center blue-text text-lighten-1 @yield('pageH1Class')">@yield('pageH1')</h1>
            <div class="row center">
                <h5 class="header col s12 light page-desc">@yield('pageDesc')</h5>
            </div>
            <div class="row center">
                @yield('top-buttons')
            </div>
            <br><br>

        </div>
    </div>
    <div class="parallax darken-5"><img class="opacity-8" src="{{ asset('background4.jpg') }}" alt=""></div>
</div>

@yield('content')

<footer class="page-footer bg-darken-blue">
    <div class="container">
        <div class="row">
            <div class="col l12">
                <h5 class="white-text">{{ trans('links.ABOUT_SITE') }}</h5>
                <p class="grey-text text-lighten-4">
                    {!! trans('links.ABOUT_SITE_TEXT') !!}
                </p>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            &copy; Valentin Sayik
        </div>
    </div>
</footer>

<script src="{{ asset('js/materialize.js') }}"></script>
<script src="{{ asset('js/init.js') }}"></script>

@yield('body')

</body>
</html>