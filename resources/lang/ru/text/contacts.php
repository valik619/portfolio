<p>Надеюсь вы ищете меня не потому что я вам что то сделал, и по этому оставляю тут свои контакты :)</p>

<p>Найти меня можно на следующих интернет ресурсах:</p>

<div class="row">

    <div class="col s6 m12">
        <ul>
            <li><b>Социальные сети:</b></li>
            <li><a href="http://vk.com/sv_backend1">ВКонтакте</a></li>
            <li><a href="https://www.facebook.com/SaikValentyn">Facebook</a></li>
            <li><a href="http://twitter.com/valik619">Twitter</a></li>
        </ul>
    </div>

    <div class="col s6 m12">
        <ul>
            <li><b>Інше:</b></li>
            <li><a href="https://github.com/valik619">GitHub</a></li>
            <li><a href="https://bitbucket.org/valik619/">BitBucket</a></li>
        </ul>
    </div>

</div>

<?php return '' ?>