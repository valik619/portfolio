<?php

return [
    'ABOUT' => 'About',
    'CONTACTS' => 'Contacts',
    'BLOG' => 'Blog',
    'WELCOME' => 'Hello! I\'m Valentin Sayik, a back-end developer from Ukraine, currently working at Find Out as the Software Engineer.',
    'SKILLS' => 'Skills',
    'EXAMPLES' => 'Examples',
    'CONTACT' => 'Contact',
    'NAME' => 'Valentin Sayik',
    'LAST_WORKS' => 'Examples',
    'INFO' => 'Information',
    'MORE' => 'More',
    'PROJECT_PAGE' => 'At this page you may find information about :name project.',
    'SCREENS' => 'Screenshots',
    'DESCRIPTION' => 'Description',

    'CONTACTS_PAGE' => 'There you find my contacts',
    'ABOUT_PAGE' => 'A little bit of information about me',
    'ABOUT_ME' => 'About Me',

    'EXAMPLES_PAGE' => 'List of projects where I take part',

    'LANGUAGES' => 'Languages',
    'FRAMEWORKS' => 'Frameworks',

    'JS_FRAMEWORKS' => 'Javascript Libraries',
    'CSS_FRAMEWORKS' => 'CSS Frameworks',
    'CMS' => 'CMS',

    'WEB_TECH' => 'Web Technologies',
    'OTHER_SKILLS' => 'Other great tools I\'ve worked with',

    'READ_ARTICLE' => 'Read more...',
    'BLOG_PAGE' => 'I write or translate various interesting and useful articles on web development',

    'ABOUT_SITE' => 'Where I am?',
    'ABOUT_SITE_TEXT' => '
    This is the site of another back-end developer, which includes 2 parts - personal blog and portfolio.
     <br><b>Blog</b> about web programming. I am writing tutorial, sharing my little experience.
     <br><b>Portfolio</b> containing all the projects in which I participate, my contacts and skills.
    ',

];
