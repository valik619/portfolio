<p>I hope you are looking for me not because I have to do something bad, and leave my contacts here :)</p>

<p>You can find me on the following Internet resources:</p>

<div class="row">

    <div class="col s6 m12">
        <ul>
            <li><b>Social Networks:</b></li>
            <li><a href="http://vk.com/sv_backend1">VKontakte</a></li>
            <li><a href="https://www.facebook.com/SaikValentyn">Facebook</a></li>
            <li><a href="http://twitter.com/valik619">Twitter</a></li>
        </ul>
    </div>

    <div class="col s6 m12">
        <ul>
            <li><b>Other:</b></li>
            <li><a href="https://github.com/valik619">GitHub</a></li>
            <li><a href="https://bitbucket.org/valik619/">BitBucket</a></li>
        </ul>
    </div>

</div>

<?php return '' ?>