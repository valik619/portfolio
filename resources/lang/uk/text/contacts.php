<p>Сподіваюсь ви шукаєте мене не через те що я вам щось зробив, а тому залишаю тут свої контакти :)</p>

<p>Знайти мене можна на наступних інтернет ресурсах:</p>

<div class="row">

    <div class="col s6 m12">
        <ul>
            <li><b>Соціальні мережі:</b></li>
            <li><a href="http://vk.com/sv_backend1">ВКонтакті</a></li>
            <li><a href="https://www.facebook.com/SaikValentyn">Facebook</a></li>
            <li><a href="http://twitter.com/valik619">Twitter</a></li>
        </ul>
    </div>

    <div class="col s6 m12">
        <ul>
            <li><b>Інше:</b></li>
            <li><a href="https://github.com/valik619">GitHub</a></li>
            <li><a href="https://bitbucket.org/valik619/">BitBucket</a></li>
        </ul>
    </div>

</div>

<?php return '' ?>