-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 19 2015 г., 14:45
-- Версия сервера: 5.5.45
-- Версия PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `sv`
--

-- --------------------------------------------------------

--
-- Структура таблицы `administrators`
--

CREATE TABLE IF NOT EXISTS `administrators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `administrators_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `administrators`
--

INSERT INTO `administrators` (`id`, `username`, `password`, `name`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$g4FTfySznq.uSu97iIcCPewLaUjrYmMOKTnJPoMsveXgYd0cgThFa', 'admin', NULL, '2015-12-19 09:45:07', '2015-12-19 09:45:07');

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `article_screens`
--

CREATE TABLE IF NOT EXISTS `article_screens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) unsigned NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `article_tags`
--

CREATE TABLE IF NOT EXISTS `article_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `article_trans`
--

CREATE TABLE IF NOT EXISTS `article_trans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) unsigned NOT NULL,
  `locale` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `comments` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `article_trans_locale_index` (`locale`),
  KEY `article_trans_name_index` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `blog_tags`
--

CREATE TABLE IF NOT EXISTS `blog_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_tags_name_index` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_10_12_104748_create_administrators_table', 1),
('2015_10_25_211512_create_projects_table', 1),
('2015_11_20_165636_create_project_tags_table', 1),
('2015_12_17_184128_create_blog_tables', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `price` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `price`, `time`, `year`, `created_at`, `updated_at`) VALUES
(1, 267, 2734, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(2, 925, 2810, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(3, 238, 1074, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(4, 205, 2185, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(5, 711, 4985, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(6, 259, 3360, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(7, 1000, 3123, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(8, 723, 3816, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(9, 892, 4219, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07');

-- --------------------------------------------------------

--
-- Структура таблицы `project_screens`
--

CREATE TABLE IF NOT EXISTS `project_screens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `project_screens`
--

INSERT INTO `project_screens` (`id`, `project_id`, `filename`, `width`, `height`, `created_at`, `updated_at`) VALUES
(1, 1, 'background4.jpg', 0, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(2, 2, 'background1.jpg', 0, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(3, 3, 'background3.jpg', 0, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(4, 4, 'background2.jpg', 0, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(5, 5, 'background3.jpg', 0, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(6, 6, 'background3.jpg', 0, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(7, 7, 'background1.jpg', 0, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(8, 8, 'background3.jpg', 0, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(9, 9, 'background1.jpg', 0, 0, '2015-12-19 09:45:07', '2015-12-19 09:45:07');

-- --------------------------------------------------------

--
-- Структура таблицы `project_tags`
--

CREATE TABLE IF NOT EXISTS `project_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `project_trans`
--

CREATE TABLE IF NOT EXISTS `project_trans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `locale` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `developers` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `project_trans_locale_index` (`locale`),
  KEY `project_trans_name_index` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Дамп данных таблицы `project_trans`
--

INSERT INTO `project_trans` (`id`, `project_id`, `locale`, `name`, `short_desc`, `text`, `url`, `developers`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 1, 'uk', 'N8sKrJ DJdV', '', 'A5KR dJHcl9 T7pTGXk9.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(2, 1, 'en', 'pS7K9 w0bHLy', '', 'nuLeg Nczc4KE HfFN.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(3, 1, 'ru', 'r4y5 4hVZnC', '', 'WKZez mpwp15 1Blx3.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(4, 2, 'uk', 'TAcTHOzK R0qQ', '', '3luR3jdf eypDleG i7fPQRG2.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(5, 2, 'en', 'DQLR9 WPfiy2u', '', 'uJlME 3dz2rBd 9SaZ.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(6, 2, 'ru', 'brPqvLku 7eMHjQ0', '', 'DMPgZ gJuR GRBI.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(7, 3, 'uk', 'hIoyK3z 4shV', '', 'ymEWQPR KAYAiI Rl7iXX6.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(8, 3, 'en', 'DIApmlSZ JTHVdnD', '', 'i6aelhA ufpw766 KvVA1.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(9, 3, 'ru', 'n9bgOn3 0979eSbO', '', 'e728 qdec yWs0ECcW.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(10, 4, 'uk', '9sXj jEG3yrAg', '', 'i235bA zEcW q49SXy.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(11, 4, 'en', 'wyJp jNvPC', '', 'mGdw75k tpX0ax QjknHpR.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(12, 4, 'ru', 'YdF8h0 6Cfe6', '', 'ZnnH ZZOR5EGr g0nMX.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(13, 5, 'uk', 'NN0PZXTY UYyE4K', '', 'lmG2iSud VS3uf gOzv.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(14, 5, 'en', 'ttkD wXxGZB', '', 'w5KA8d vOYQ9 l24KI.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(15, 5, 'ru', 'pLP2 oaob', '', 'x3V2Jc G7NVFe3R UdYu.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(16, 6, 'uk', 'mp8OZSa rPZEAeI', '', 'Uy6Teuqc Hqt7PO PIIN.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(17, 6, 'en', 'lHqp CYoHMwhF', '', 'y0LXg 2QQI fe77n.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(18, 6, 'ru', 'sbZkg prPclG', '', '8VyHY NHU64e Xjulyg8.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(19, 7, 'uk', 'traf12Pd eXjnfs8W', '', 'UppcD3G1 GRqG4KaQ NND9E.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(20, 7, 'en', 'z2k2xQm5 kJgr9cx', '', 'kMim75g QRwmub q9lv8P.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(21, 7, 'ru', 'SvOiRF uinT6C68', '', 'egs0C cHaFHu1E 8ZtTfk.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(22, 8, 'uk', 'MCIdKGP 71Goia', '', 'mI71V CV7T pzHhYO2.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(23, 8, 'en', 'zdfQpM5M fBAWv7', '', 'SGvKRW3 tqH2fN vQTk.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(24, 8, 'ru', 'JfQtQgJ5 J0mCb', '', '9sP2nu JeXN qnhcu.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(25, 9, 'uk', 'mNT2aunJ IVtZ', '', 'iyVW 97xf NBkVHQqE.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(26, 9, 'en', 'Ws82e8p DKHmDJ', '', 'Ua0Q F9W99N 7t2nN.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07'),
(27, 9, 'ru', 'BHtYomyi TQfvnL5', '', 'Ny8u1H uynRk 8iC9.', '', '', '', '', '2015-12-19 09:45:07', '2015-12-19 09:45:07');

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tags_name_index` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
