<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = ['uk', 'en', 'ru'];

        DB::table('administrators')->insert([
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'name' => 'admin',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        for($i = 0; $i++<=8;) {
            DB::table('projects')->insert([
                'price' => mt_rand(100, 1000),
                'time' => mt_rand(1000, 5000),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        for($i = 0; $i++<=8;) {
            DB::table('project_screens')->insert([
                'project_id' => $i,
                'filename' => 'background'.random_int(1,4).'.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        for($i = 0; $i++<=8;) {
            foreach ($locales as $locale) {
                DB::table('project_trans')->insert([
                    'project_id' => $i,
                    'locale' => $locale,
                    'name' => str_random(mt_rand(4,8)).' '.str_random(mt_rand(4,8)),
                    'text' => str_random(mt_rand(4,8)).' '.str_random(mt_rand(4,8)).' '.str_random(mt_rand(4,8)).'.',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }

    }
}
