<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('article_trans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id')->unsigned();
            $table->string('locale', 2)->index();
            $table->string('name')->index();
            $table->string('short_desc');
            $table->text('text');
            $table->string('meta_keywords');
            $table->text('meta_description');
            $table->integer('comments');
            $table->timestamps();
        });

        Schema::create('article_screens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id')->unsigned();
            $table->string('filename');
            $table->integer('width');
            $table->integer('height');
            $table->timestamps();
        });

        Schema::create('article_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id');
            $table->integer('tag_id');
            $table->timestamps();
        });

        Schema::create('blog_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150)->index();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
        Schema::drop('article_trans');
        Schema::drop('article_screens');
        Schema::drop('article_tags');
        Schema::drop('blog_tags');
    }
}
