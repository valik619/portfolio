<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Meta Information
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('price');
            $table->integer('time');
            $table->integer('year');
            $table->timestamps();
        });

        //Translated Info
        Schema::create('project_trans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->string('locale', 2)->index();

            $table->string('name')->index();
            $table->string('short_desc');
            $table->text('text');

            $table->string('url');
            $table->string('developers');

            $table->string('meta_keywords');
            $table->text('meta_description');

            $table->timestamps();
        });

        //Screenshots
        Schema::create('project_screens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->string('filename');
            $table->integer('width');
            $table->integer('height');
            $table->timestamps();


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_trans');
        Schema::drop('project_screens');

        Schema::drop('projects');
        Schema::drop('tags');
    }
}
