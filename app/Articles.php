<?php

namespace App;

use Anakadote\ImageManager\Facades\ImageManager;
use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $table = 'articles';

    use \Dimsav\Translatable\Translatable;

    public $translationModel = 'App\ArticlesTranslate';

    public $translatedAttributes = [
        'name',
        'text',
        'short_desc',
        'meta_description',
        'meta_keywords',
    ];

    public $translationForeignKey = 'article_id';

    public function screen()
    {
        return $this->hasMany('App\ArticlesScreen', 'article_id', 'id');
    }

    public static function getArticlesByTag($tag_id)
    {
        $articles_id = ArticlesTag::where('tag_id', $tag_id)->groupBy('article_id')->get();
        $ids = [];

        foreach ($articles_id as $relation) {
            array_push($ids, $relation->article_id);
        }

        return Articles::find($ids);
    }

    public static function getTagsByArticle($article_id)
    {
        $articles_id = ArticlesTag::where('article_id', $article_id)->groupBy('tag_id')->get();
        $ids = [];

        foreach ($articles_id as $relation) {
            array_push($ids, $relation->tag_id);
        }

        return BlogTag::find($ids);
    }

    /**
     * @param $id
     * @return int
     */
    public static function clear($id)
    {
        ArticlesTranslate::destroy(ArticlesTranslate::all()->where('article_id', $id)->modelKeys());

        $screens = ArticlesScreen::all()->where('article_id', $id);
        foreach ($screens as $arr) {
            ImageManager::deleteImage(public_path() . '/' . $arr->filename);
        }

        return Articles::destroy($id);

    }
}
