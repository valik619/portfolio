<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticlesTranslate extends Model
{
    protected $table = 'article_trans';
    protected $fillable = ['name', 'text'];

    /*public function translate(){
        return $this->belongsTo('Projects');
    }*/

}
