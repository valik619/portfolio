<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticlesScreen extends Model
{
    protected $table = 'article_screens';
}
