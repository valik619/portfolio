<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectsTag extends Model
{
    protected $table = 'project_tags';

    public static function clearProjectTags($project_id)
    {
        return ProjectsTag::where('project_id', $project_id)->delete();
    }

    /**
     * @param $tag_id
     * @return mixed
     */
    public static function clearProjectsTag($tag_id)
    {
        if(ProjectsTag::where('tag_id', $tag_id)->first()) {
            return ProjectsTag::where('tag_id', $tag_id)->delete();
        }else return true;
    }

    public static function relateProjectsTags($project_id, $tags = [])
    {
        $success = 0;
        if (count($tags)) {
            foreach ($tags as $tag) {
                $arr = new ProjectsTag();
                $arr->project_id = $project_id;
                $arr->tag_id = $tag;
                if ($arr->save()) $success++;
            }
            if (count($tags) == $success) {
                return true;
            } else return false;
        } else return false;
    }
}
