<?php

namespace App;

use Anakadote\ImageManager\Facades\ImageManager;
use Illuminate\Database\Eloquent\Model;

use App\ProjectsTranslate;
use App\ProjectsScreen;

class Projects extends Model
{
    protected $table = 'projects';

    use \Dimsav\Translatable\Translatable;

    public $translationModel = 'App\ProjectsTranslate';

    public $translatedAttributes = [
        'name',
        'text',
        'short_desc',
        'url',
        'developers',
        'meta_description',
        'meta_keywords',
    ];

    public $translationForeignKey = 'project_id';

    public function screen()
    {
        return $this->hasMany('App\ProjectsScreen', 'project_id', 'id');
    }

    public static function getProjectsByTag($tag_id)
    {
        $projects_id = ProjectsTag::where('tag_id', $tag_id)->groupBy('project_id')->get();
        $ids = [];

        foreach ($projects_id as $relation) {
            array_push($ids, $relation->project_id);
        }

        return Projects::find($ids);
    }

    /**
     * @param $id
     * @return int
     */
    public static function clear($id)
    {
        ProjectsTranslate::destroy(ProjectsTranslate::all()->where('project_id', $id)->modelKeys());

        $screens = ProjectsScreen::all()->where('project_id', $id);
        foreach ($screens as $arr) {
            ImageManager::deleteImage(public_path() . '/' . $arr->filename);
        }

        return Projects::destroy($id);

    }
}
