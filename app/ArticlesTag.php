<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticlesTag extends Model
{
    protected $table = 'article_tags';

    /**
     * @param $article_id
     * @return mixed
     */
    public static function clearArticleTags($article_id)
    {
        return ArticlesTag::where('article_id', $article_id)->delete();
    }

    /**
     * @param $tag_id
     * @return mixed
     */
    public static function clearArticlesTag($tag_id)
    {
        if(ArticlesTag::where('tag_id', $tag_id)->first()) {
            return ArticlesTag::where('tag_id', $tag_id)->delete();
        }else return true;
    }

    /**
     * @param $article_id
     * @param array $tags
     * @return bool
     */
    public static function relateArticlesTags($article_id, $tags = [])
    {
        $success = 0;
        if (count($tags)) {
            foreach ($tags as $tag) {
                $arr = new ArticlesTag();
                $arr->article_id = $article_id;
                $arr->tag_id = $tag;
                if ($arr->save()) $success++;
            }
            if (count($tags) == $success) {
                return true;
            } else return false;
        } else return false;
    }
}
