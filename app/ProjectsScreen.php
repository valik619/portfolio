<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectsScreen extends Model
{
    protected $table = 'project_screens';
}
