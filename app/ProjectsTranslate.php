<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectsTranslate extends Model
{
    protected $table = 'project_trans';
    protected $fillable = ['name', 'text'];

    /*public function translate(){
        return $this->belongsTo('Projects');
    }*/

}
