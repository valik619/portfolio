<?php

Route::get('', [
	'as' => 'admin.home',
	function ()
	{
		$content = 'Define your dashboard here.';
		return Admin::view($content, 'Dashboard');
	}
]);

Route::group(['prefix' => 'articles'], function()
{

	Route::get('add', [
		'as' => 'admin.Articles.DestroyTranslate',
		'uses' => '\App\Http\Controllers\Admin\Blog\IndexController@add',
	]);

	Route::post('create', [
		'as' => 'admin.Articles.AddTranslate',
		'uses' => '\App\Http\Controllers\Admin\Blog\IndexController@create',
	]);

	Route::post('add-translate/{id}', [
		'as' => 'admin.Articles.AddTranslate',
		'uses' => '\App\Http\Controllers\Admin\Blog\IndexController@addTranslate',
	]);

	Route::get('delete-translate/{id}', [
		'as' => 'admin.Articles.DestroyTranslate',
		'uses' => '\App\Http\Controllers\Admin\Blog\IndexController@deleteTranslate',
	]);

	Route::get('{id}/destroy', [
		'as' => 'admin.Articles.DestroyProject',
		'uses' => '\App\Http\Controllers\Admin\Blog\IndexController@destroy',
	]);

	Route::get('{id}/screens', [
		'as' => 'admin.Articles.Screens',
		'uses' => '\App\Http\Controllers\Admin\Blog\IndexController@screens',
	]);

	Route::get('{id}/screens/delete', [
		'as' => 'admin.Articles.Screens',
		'uses' => '\App\Http\Controllers\Admin\Blog\IndexController@screenDelete',
	]);

	Route::get('{id}/screens/new', [
		'as' => 'admin.Articles.Screens',
		'uses' => '\App\Http\Controllers\Admin\Blog\IndexController@screenNew',
	]);

	Route::post('{id}/screens/save-screen', [
		'as' => 'admin.Articles.ScreenSave',
		'uses' => '\App\Http\Controllers\Admin\Blog\IndexController@screenSave',
	]);

});

Route::resource('articles', '\App\Http\Controllers\Admin\Blog\IndexController');

Route::group(['prefix' => 'projects'], function()
{

	Route::get('add', [
		'as' => 'admin.Projects.DestroyTranslate',
		'uses' => '\App\Http\Controllers\Admin\Portfolio\IndexController@add',
	]);

	Route::post('create', [
		'as' => 'admin.Projects.AddTranslate',
		'uses' => '\App\Http\Controllers\Admin\Portfolio\IndexController@create',
	]);

	Route::post('add-translate/{id}', [
		'as' => 'admin.Projects.AddTranslate',
		'uses' => '\App\Http\Controllers\Admin\Portfolio\IndexController@addTranslate',
	]);

	Route::get('delete-translate/{id}', [
		'as' => 'admin.Projects.DestroyTranslate',
		'uses' => '\App\Http\Controllers\Admin\Portfolio\IndexController@deleteTranslate',
	]);

	Route::get('{id}/destroy', [
		'as' => 'admin.Projects.DestroyProject',
		'uses' => '\App\Http\Controllers\Admin\Portfolio\IndexController@destroy',
	]);

	Route::get('{id}/screens', [
		'as' => 'admin.Projects.Screens',
		'uses' => '\App\Http\Controllers\Admin\Portfolio\IndexController@screens',
	]);

	Route::get('{id}/screens/delete', [
		'as' => 'admin.Projects.Screens',
		'uses' => '\App\Http\Controllers\Admin\Portfolio\IndexController@screenDelete',
	]);

	Route::get('{id}/screens/new', [
		'as' => 'admin.Projects.Screens',
		'uses' => '\App\Http\Controllers\Admin\Portfolio\IndexController@screenNew',
	]);

	Route::post('{id}/screens/save-screen', [
		'as' => 'admin.Projects.ScreenSave',
		'uses' => '\App\Http\Controllers\Admin\Portfolio\IndexController@screenSave',
	]);

});

Route::resource('projects', '\App\Http\Controllers\Admin\Portfolio\IndexController');

Route::group(['prefix' => 'tags'], function() {

	Route::get('', [
		'uses' => '\App\Http\Controllers\Admin\Portfolio\TagController@index',
	]);

	Route::get('/add', [
		'uses' => '\App\Http\Controllers\Admin\Portfolio\TagController@add',
	]);

	Route::post('create', [
		'uses' => '\App\Http\Controllers\Admin\Portfolio\TagController@create',
	]);

	Route::get('{id}/destroy', [
		'uses' => '\App\Http\Controllers\Admin\Portfolio\TagController@destroy',
	]);

	Route::get('{id}/edit', [
		'uses' => '\App\Http\Controllers\Admin\Portfolio\TagController@edit',
	]);

	Route::post('{id}/save', [
		'uses' => '\App\Http\Controllers\Admin\Portfolio\TagController@save',
	]);
});


Route::group(['prefix' => 'blog/tags'], function() {

	Route::get('', [
		'uses' => '\App\Http\Controllers\Admin\Blog\TagController@index',
	]);

	Route::get('/add', [
		'uses' => '\App\Http\Controllers\Admin\Blog\TagController@add',
	]);

	Route::post('create', [
		'uses' => '\App\Http\Controllers\Admin\Blog\TagController@create',
	]);

	Route::get('{id}/destroy', [
		'uses' => '\App\Http\Controllers\Admin\Blog\TagController@destroy',
	]);

	Route::get('{id}/edit', [
		'uses' => '\App\Http\Controllers\Admin\Blog\TagController@edit',
	]);

	Route::post('{id}/save', [
		'uses' => '\App\Http\Controllers\Admin\Blog\TagController@save',
	]);
});