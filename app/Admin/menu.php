<?php

Admin::menu()->label('Blog')->icon('fa-book')->items(function ()
{
    Admin::menu(\app\Projects::class)->url('articles')->icon('fa-book')->label('Articles');
    Admin::menu(\app\Projects::class)->url('articles/add')->icon('fa-plus')->label('Write new post');
});

Admin::menu()->label('Projects')->icon('fa-book')->items(function ()
{
    Admin::menu(\app\Projects::class)->url('projects')->icon('fa-book')->label('Projects');
    Admin::menu(\app\Projects::class)->url('projects/add')->icon('fa-plus')->label('Add new project');
});

Admin::menu()->label('Tags')->icon('fa-book')->items(function ()
{
    Admin::menu(\app\Projects::class)->url('tags')->icon('fa-book')->label('Tags');
    Admin::menu(\app\Projects::class)->url('tags/add')->icon('fa-plus')->label('Add new tag');
});

Admin::menu()->label('Blog Tags')->icon('fa-book')->items(function ()
{
    Admin::menu(\app\Projects::class)->url('blog/tags')->icon('fa-book')->label('Tags');
    Admin::menu(\app\Projects::class)->url('blog/tags/add')->icon('fa-plus')->label('Add new blog tag');
});