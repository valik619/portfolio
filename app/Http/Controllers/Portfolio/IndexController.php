<?php

namespace App\Http\Controllers\Portfolio;

use App\Http\Controllers\Controller;
use App\Projects;
use App\ProjectsTag;
use App\Tag;

#use App\ProjectsTranslate;

class IndexController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $model = Projects::all()->take(3)->sortByDesc('created_at');

        return view('portfolio.index',[
            'model' => $model,
        ]);
    }

    public function projects()
    {
        $model = Projects::all();

        return view('portfolio.projects',[
            'model' => $model,
        ]);
    }

    public function tag($tag)
    {
        $tagModel = Tag::where('name', $tag)->first();

        if(!$tagModel){
            abort('404', 'Tag not exists');
        }

        $model = Projects::getProjectsByTag($tagModel->id);

        return view('portfolio.projects',[
            'model' => $model,
        ]);

    }

    public function project($id)
    {
        $model = Projects::find($id);
        return view('portfolio.project', [
            'model' => $model,
        ]);
    }

    public function about()
    {
        return view('portfolio.about');
    }

    public function contacts()
    {
       return view('portfolio.contacts');
    }
}
