<?php

namespace App\Http\Controllers\Blog;

use App\BlogTag;
use App\Http\Controllers\Controller;
use App\Articles;

class IndexController extends Controller
{

    public function index()
    {
        $model = Articles::all()->sortByDesc('created_at');

        return view('articles.index',[
            'model' => $model,
        ]);
    }

    public function tag($tag)
    {
        $tagModel = BlogTag::where('name', $tag)->first();

        if(!$tagModel){
            abort('404', trans('links.TAG_NOT_EXISTS'));
        }

        $model = Articles::getArticlesByTag($tagModel->id);

        return view('articles.index',[
            'model' => $model,
        ]);

    }

    public function article($id)
    {

        $article = Articles::find($id);

        if(!$article){
            abort(404, trans('links.ARTICLE_NOT_FOUND'));
        }

        return view('articles.article', [
            'article' => $article,
        ]);
    }

}
