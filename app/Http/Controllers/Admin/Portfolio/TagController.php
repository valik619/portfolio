<?php

namespace App\Http\Controllers\Admin\Portfolio;

use App\ProjectsTag;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Projects;
use App\Tag;
use SleepingOwl\Admin\Admin;
use Config;

use Input;
use Validator;
use Redirect;
use Session;
use Storage;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Tag::all();
        return Admin::view(view('admin/portfolio/tags', [
            'model' => $model,
        ]), 'Tags');
    }

    public function add(){
        return Admin::view(view('admin/portfolio/new_tag'), 'Create Tag');
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id){
        $model = Tag::find($id);
        return Admin::view(view('admin/portfolio/edit_tag',[
            'model' => $model,
            'id' => $id,
        ]), 'Edit Tag');
    }

    /**
     * @param Request $request
     * @return Redirect
     */
    public function save(Request $request, $id){

        $tag = Tag::find($id);
        $tag->name = $request->name;

        if($tag->save())
            return redirect(url('admin/tags'));
        else
            return redirect(url('admin/tags/add'));
    }

    /**
     * @param Request $request
     * @return Redirect
     */
    public function create(Request $request){

        $tag = new Tag();
        $tag->name = $request->name;

        if($tag->save())
            return redirect(url('admin/tags'));
        else
            return redirect(url('admin/tags/add'));
    }


    public function destroy($id){
        if(ProjectsTag::clearProjectsTag($id) && Tag::find($id)->delete()){
            $message = 'Success!';
        }else{
            $message = 'Error!';
        }
        return redirect(url('admin/tags'))->with('message', $message);
    }


}
