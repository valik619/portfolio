<?php

namespace App\Http\Controllers\Admin\Portfolio;

use App\ProjectsScreen;
use App\ProjectsTag;
use App\ProjectsTranslate;
use App\Tag;

use Anakadote\ImageManager\Facades\ImageManager;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Projects;
use SleepingOwl\Admin\Admin;
use Config;

use Input;
use Validator;
use Redirect;
use Session;

use Storage;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Projects::all()->sortByDesc('created_at');
        return Admin::view(view('admin/portfolio/index', [
            'model' => $model,
        ]), 'Projects');
    }

    public function add(){
        return Admin::view(view('admin/portfolio/new_project'), 'Create Project');
    }

    /**
     * @param Request $request
     * @return Redirect
     */
    public function create(Request $request){

        $time_val = 3600;

        if($request->time_type == 'D')
            $time_val = 3600*24;

        if($request->time_type == 'M')
            $time_val = 3600*24*30;

        /**
         * @var $project Object Projects
         * @var $request Request
         */

        $project = new Projects();
        $project->price = $request->price;
        $project->time = ($request->time*$time_val);
        $project->year = $request->year;

        if($project->save())
            return redirect(url('admin/projects/'.$project->id.'/edit'));
        else
            return redirect(url('admin/projects/add'));
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function screens($id)
    {
        $model = ProjectsScreen::where('project_id', $id)->get();

        return Admin::view(view('admin/portfolio/screens', [
            'model' => $model,
            'id' => $id,
        ]), 'Screens');
    }

    public function screenDelete($id)
    {
        $model = ProjectsScreen::find($id);
        $id = $model->project_id;

        ImageManager::deleteImage(public_path().'/screens/'.$id.'/'.$model->filename);

        $model->delete($model->id);

        return redirect(url('admin/projects/'.$id.'/screens'));
    }


    public function screenNew($id)
    {
        return Admin::view(view('admin/portfolio/screen_new', [
            'id' => $id,
        ]), 'Screen new');
    }

    public function screenSave(Request $request, $id)
    {
        $files = Input::file('screen');
        $file_count = count($files);
        $uploadcount = 0;

        foreach($files as $file) {
            $rules = array('file' => 'required|mimes:png,jpeg,jpg');
            $validator = Validator::make(array('file'=> $file), $rules);
            if($validator->passes()){

                $destinationPath = 'screens/'.$id;
                $filename = str_random(3).'.png';
                $sizes = getimagesize($file);

                if($file->move($destinationPath, $filename)){
                    $screen = new ProjectsScreen;
                    $screen->project_id = $id;
                    $screen->filename = $filename;
                    $screen->width = $sizes[0];
                    $screen->height = $sizes[1];
                    $screen->save();
                }

                $uploadcount++;
            }
        }
        if($uploadcount == $file_count){
            Session::flash('message', 'Uploaded '.$file_count.' files successfully');
            return redirect(url('admin/projects/'.$id.'/screens/new'));
        }
        else {
            Session::flash('message', 'Error!');
            return redirect(url('admin/projects/'.$id.'/edit'));
        }

    }



    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = ProjectsTranslate::where('project_id', $id)->get();

        $tags = ProjectsTag::where('project_id', $id)->get();
        $all_tags = Tag::all();
        $checked_tags = [];

        foreach ($tags as $tag) {
            $checked_tags[$tag->tag_id] = $tag->tag_id;
        }


        return Admin::view(view('admin/portfolio/edit', [
            'model' => $model,
            'tags' => $checked_tags,
            'all_tags' => $all_tags,
            'id' => $id,
        ]), 'Edit');

    }

    /**
     * Update the specified resource in storage.
     *
     * @var $arr ProjectsTranslate
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $translates = ProjectsTranslate::where('project_id', $id)->get();
        $message = 'Save Success!';

        if(ProjectsTag::relateProjectsTags($id, $request->tags) === false){
            $message = 'Tags not related!';
        }

        foreach($translates as $arr){
            $arr->name = $request->name[$arr->locale];
            $arr->text = $request->text[$arr->locale];

            $arr->url = $request->url[$arr->locale];
            $arr->developers = $request->developers[$arr->locale];
            $arr->short_desc = $request->short_desc[$arr->locale];
            $arr->meta_keywords = $request->meta_keywords[$arr->locale];
            $arr->meta_description = $request->meta_description[$arr->locale];

            if(!$arr->save()) $message = 'Translate not saved!';
        }

        return redirect(url('admin/projects/'.$id.'/edit'))->with('message', $message);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function addTranslate(Request $request, $id)
    {
        /**
         * @var $request Object Request
         * @var $translate object ProjectsTranslate
         */

        if (in_array($request->locale, Config::get('app.locales'))) {
            $translate = new ProjectsTranslate();
            $translate->project_id = $id;
            $translate->text = $request->locale;
            $translate->name = $request->locale;
            $translate->locale = $request->locale;
            $translate->save();
            $message = 'Translate successfully added!';
        }else $message = 'ERROR!';
        return redirect(url('admin/projects/'.$id.'/edit'))->with('message', $message);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Redirect
     */
    public function deleteTranslate(Request $request, $id)
    {
        /**
         * @var $model Object ProjectsTranslate
         */
        $model = ProjectsTranslate::find($id);
        $id = $model->project_id;
        $model->destroy($model->id);
        #ProjectsTranslate::destroy($id);
        return redirect(url('admin/projects/'.$id.'/edit'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Projects::clear($id);

        return redirect(url('admin/projects'));
    }
}
