<?php

namespace App\Http\Controllers\Admin\Blog;

use App\ArticlesTag;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\BlogTag;
use SleepingOwl\Admin\Admin;

use Redirect;


class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = BlogTag::all();
        return Admin::view(view('admin/articles/tags', [
            'model' => $model,
        ]), 'Tags');
    }

    public function add(){
        return Admin::view(view('admin/articles/new_tag'), 'Create Tag');
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id){
        $model = BlogTag::find($id);
        return Admin::view(view('admin/articles/edit_tag',[
            'model' => $model,
            'id' => $id,
        ]), 'Edit Tag');
    }

    /**
     * @param Request $request
     * @return Redirect
     */
    public function save(Request $request, $id){

        $tag = BlogTag::find($id);
        $tag->name = $request->name;

        if($tag->save())
            return redirect(url('admin/blog/tags'));
        else
            return redirect(url('admin/blog/tags/add'));
    }

    /**
     * @param Request $request
     * @return Redirect
     */
    public function create(Request $request){

        $tag = new BlogTag();
        $tag->name = $request->name;

        if($tag->save())
            return redirect(url('admin/blog/tags'));
        else
            return redirect(url('admin/blog/tags/add'));
    }


    public function destroy($id){
        if(ArticlesTag::clearArticlesTag($id) && BlogTag::find($id)->delete()){
            $message = 'Success!';
        }else{
            $message = 'Error!';
        }
        return redirect(url('admin/blog/tags'))->with('message', $message);
    }


}
