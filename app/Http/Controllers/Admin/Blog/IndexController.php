<?php

namespace App\Http\Controllers\Admin\Blog;

use App\ArticlesScreen;
use App\ArticlesTag;
use App\ArticlesTranslate;
use App\BlogTag;

use Anakadote\ImageManager\Facades\ImageManager;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Articles;
use SleepingOwl\Admin\Admin;
use Config;

use Input;
use Validator;
use Redirect;
use Session;


class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Articles::all()->sortByDesc('created_at');
        return Admin::view(view('admin/articles/index', [
            'model' => $model,
        ]), 'Articles');
    }

    public function add(){

        $article = new Articles();

        if($article->save())
            return redirect(url('admin/articles/'.$article->id.'/edit'));
        else
            return Admin::view(view('admin/articles/'), 'Articles');
    }

    /**
     * @param Request $request
     * @return Redirect
     */
    public function create(Request $request){

        $article = new Articles();

        if($article->save())
            return redirect(url('admin/articles/'.$article->id.'/edit'));
        else
            return redirect(url('admin/articles/add'));
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function screens($id)
    {
        $model = ArticlesScreen::where('article_id', $id)->get();

        return Admin::view(view('admin/articles/screens', [
            'model' => $model,
            'id' => $id,
        ]), 'Screens');
    }

    public function screenDelete($id)
    {
        $model = ArticlesScreen::find($id);
        $id = $model->article_id;

        ImageManager::deleteImage(public_path().'/pictures/'.$id.'/'.$model->filename);

        $model->delete($model->id);

        return redirect(url('admin/articles/'.$id.'/screens'));
    }


    public function screenNew($id)
    {
        return Admin::view(view('admin/articles/screen_new', [
            'id' => $id,
        ]), 'Screen new');
    }

    public function screenSave(Request $request, $id)
    {
        $files = Input::file('screen');
        $file_count = count($files);
        $uploadcount = 0;

        foreach($files as $file) {
            $rules = array('file' => 'required|mimes:png,jpeg,jpg');
            $validator = Validator::make(array('file'=> $file), $rules);
            if($validator->passes()){

                $destinationPath = 'pictures/'.$id;
                $filename = str_random(3).'.png';
                $sizes = getimagesize($file);

                if($file->move($destinationPath, $filename)){
                    $screen = new ArticlesScreen();
                    $screen->article_id = $id;
                    $screen->filename = $filename;
                    $screen->width = $sizes[0];
                    $screen->height = $sizes[1];
                    $screen->save();
                }

                $uploadcount++;
            }
        }
        if($uploadcount == $file_count){
            Session::flash('message', $file_count.' files successfully uploaded!');
            return redirect(url('admin/articles/'.$id.'/screens/new'));
        }
        else {
            Session::flash('message', 'Error!');
            return redirect(url('admin/articles/'.$id.'/edit'));
        }

    }



    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = ArticlesTranslate::where('article_id', $id)->get();

        $tags = ArticlesTag::where('article_id', $id)->get();
        $all_tags = BlogTag::all();
        $checked_tags = [];

        foreach ($tags as $tag) {
            $checked_tags[$tag->tag_id] = $tag->tag_id;
        }

        return Admin::view(view('admin/articles/edit', [
            'model' => $model,
            'tags' => $checked_tags,
            'all_tags' => $all_tags,
            'id' => $id,
        ]), 'Edit');

    }

    /**
     * Update the specified resource in storage.
     *
     * @var $arr ArticlesTranslate
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $translates = ArticlesTranslate::where('article_id', $id)->get();
        $message = 'Save Success!';

        if(ArticlesTag::relateArticlesTags($id, $request->tags) === false){
            $message = 'Tags not related!';
        }

        foreach($translates as $arr){
            $arr->name = $request->name[$arr->locale];
            $arr->text = $request->text[$arr->locale];
            $arr->short_desc = $request->short_desc[$arr->locale];
            $arr->meta_keywords = $request->meta_keywords[$arr->locale];
            $arr->meta_description = $request->meta_description[$arr->locale];

            if(!$arr->save()) $message = 'Translate not saved!';
        }

        return redirect(url('admin/articles/'.$id.'/edit'))->with('message', $message);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function addTranslate(Request $request, $id)
    {
        /**
         * @var $request Object Request
         * @var $translate object ArticleTranslate
         */

        if (in_array($request->locale, Config::get('app.locales'))) {
            $translate = new ArticlesTranslate();
            $translate->article_id = $id;
            $translate->text = $request->locale;
            $translate->name = $request->locale;
            $translate->locale = $request->locale;
            $translate->save();
            $message = 'Translate added successfully!';
        }else $message = 'ERROR!';
        return redirect(url('admin/articles/'.$id.'/edit'))->with('message', $message);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Redirect
     */
    public function deleteTranslate(Request $request, $id)
    {
        /**
         * @var $model Object ArticlesTranslate
         */
        $model = ArticlesTranslate::find($id);
        $id = $model->article_id;
        $model->destroy($model->id);
        return redirect(url('admin/articles/'.$id.'/edit'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Articles::clear($id);

        return redirect(url('admin/articles'));
    }
}
