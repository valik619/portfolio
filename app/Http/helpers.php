<?php

function urlTo($route = '/'){
    return Linguist::url($route);
}

function image($path = '/background1.jpg', $width = 0, $height = 0, $full_width = 0, $full_height = 0, $type = 'crop'){

    if(!$height && !$width){
        throw new Exception('Missing required attribute: $width and $height');
    }

    if(!$height){
        $ratio = $full_height / $full_width;
        $height = round($width * $ratio);
    }

    if(!$width){
        $ratio = $full_width / $full_height;
        $width = round($height * $ratio);
    }

    return ImageManager::getImagePath(public_path() . $path, $width, $height, $type);
}
